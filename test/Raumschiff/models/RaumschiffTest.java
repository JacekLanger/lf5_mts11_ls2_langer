package Raumschiff.models;

import Raumschiff.utils.RaumschiffUtils;
import java.util.List;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

class RaumschiffTest {

  @Test
  void photonenTorpedosAbfeuern() {

  }

  @Test
  void phaserAbfeuern() {

  }

  @Test
  void photonenTorpedoLaden() {


  }

  @Test
  void reparieren() {

  }

  @Test
  void zustandAusgaben() {

  }

  @Test
  void beladen() {

    final Raumschiff r1 = new Raumschiff();

    Assertions.assertTrue(r1.getLadungsverzeichniss().isEmpty());
    r1.beladen(new PhotonenTorpedo(3));
    Assertions.assertTrue(
        r1.getLadungsverzeichniss().containsKey(RaumschiffUtils.PHOTONEN_TORPEDOS));
  }


  @Test
  void nachrichtSenden() {

    final var list = List.of("hallo", "und", "tschüss");

    for (final String s : list) {
      if (s.equals("hello")) {
        System.out.println(s);
      }
    }
    list.stream().filter(s -> s.equals("hallo")).forEach(System.out::println);
  }

  @Test
  void logbuchEintraegeAusgeben() {

  }

}