package Raumschiff.models;

import java.rmi.server.UID;
import java.util.ArrayList;
import java.util.List;

/**
 * Exception fuer Invalide Methoden Ausfuehrung.
 */
public class InvalidOperationException extends Exception {

  //  private static final Logger LOGGER = Logger.getLogger(InvalidOperationException.class
  //  .getName());
  private static final List<String> LOGS = new ArrayList<>();
  private static final UID serialVersionUID = new UID();

  /**
   * Erstellt eine neue Instanz der Exception und Fuegt die Nachricht der Liste der Vorhandenen
   * Lognachrichten hinzu.
   *
   * @param s die Nachricht
   */
  public InvalidOperationException(final String s) {

    super(s);
    //    LOGGER.log(Level.WARNING, s);
    LOGS.add(s);
  }

  /**
   * Speichert die Lognachrichten in einer Logdatei ab.
   */
  public void printLogs() {

  }

}
