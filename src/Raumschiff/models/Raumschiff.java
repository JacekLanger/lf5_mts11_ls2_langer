package Raumschiff.models;

import Raumschiff.utils.ConsoleUtils;
import Raumschiff.utils.RaumschiffUtils;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.Observer;
import java.util.Random;

/**
 * Raumschiff Klasse.
 */
public class Raumschiff extends PhotonenTorpedo {

  /**
   * Das Brodcast-System des Raumschiffs. Es wird mit allen Raumschiffen geteilt.
   */
  private static final List<String> BROADCAST_COMMUNICATOR = new ArrayList<>();
  /**
   * Name des Raumschiffs.
   */
  private String name;
  /**
   * Zustand der Huelle in Prozent.
   */
  private double huelle;
  /**
   * Zustand der Schilde in Prozent.
   */
  private double schilde;
  /**
   * Zustand der Lebenserhaltung in Prozent.
   */
  private double lebenserhaltung;
  /**
   * Zustand der Energieversorgung in Prozent.
   */
  private double energieversorgung;
  /**
   * Anzahl der geladenen Photonentorpedos.
   */
  private int photonenTorpedos;
  /**
   * Anzahl der verfuegbaren Reperaturandroiden.
   */
  private int reperaturAndroiden;
  /**
   * Das Ladungsverzeichniss, Ladungen können mittels Namen abgerufen werden.
   */
  private Map<String, Ladung> ladungsverzeichniss;

  /**
   * Gibt den Namen des Raumschiffs zurueck.
   *
   * @return name
   */
  public String getName() {

    return name;
  }

  /**
   * Setzt den namen des Raumschiffs.
   *
   * @param name neuer name
   */
  public void setName(final String name) {

    this.name = name;
  }

  /**
   * Gibt den Zustand der Huelle zurueck.
   *
   * @return huelle
   */
  public double getHuelle() {

    return huelle;
  }

  /**
   * Setzt den zustand der Huelle.
   *
   * @param huelle neuer zustand fuer die Huelle. 0-1
   */
  public void setHuelle(final double huelle) {

    this.huelle = huelle;
  }

  /**
   * Gibt den Zustand der Schilde wieder.
   *
   * @return schilde
   */
  public double getSchilde() {

    return schilde;
  }

  /**
   * Setzt den Zustand der Schilde.
   *
   * @param schilde neuer wert fuer Schilde. 0-1
   */
  public void setSchilde(final double schilde) {

    this.schilde = schilde;
  }

  /**
   * Gibt den Zustand der Lebenserhaltung zurueck.
   *
   * @return lebenserhaltung
   */
  public double getLebenserhaltung() {

    return lebenserhaltung;
  }

  /**
   * Setzt den Zustand der Lebenserhaltung.
   *
   * @param lebenserhaltung neuer wert fuer Lebenserhaltung. 0-1
   */
  public void setLebenserhaltung(final double lebenserhaltung) {

    this.lebenserhaltung = lebenserhaltung;
  }

  /**
   * Gibt den Zustand der Energieversorgung zurueck.
   *
   * @return energieversorgung
   */
  public double getEnergieversorgung() {

    return energieversorgung;
  }

  /**
   * Setzt einen neuen wert fuer die Energieversorgung.
   *
   * @param energieversorgung neuer Wert fuer Energieversorgung. 0-1
   */
  public void setEnergieversorgung(final double energieversorgung) {

    this.energieversorgung = energieversorgung;
  }

  /**
   * Gibt das Ladungsverzeichniss des Schiffs zurueck.
   *
   * @return {link ladungsverzeichniss}
   */
  public Map<String, Ladung> getLadungsverzeichniss() {

    return ladungsverzeichniss;
  }

  /**
   * Setzt das Ladungsverzeichniss.
   *
   * @param ladungsverzeichniss neuer Wert fuer Ladungsverzeichniss
   */
  public void setLadungsverzeichniss(final Map<String, Ladung> ladungsverzeichniss) {

    this.ladungsverzeichniss = ladungsverzeichniss;
  }

  /**
   * Gibt photonentorpedos wieder.
   *
   * @return photonentorpedos
   */
  public int getPhotonenTorpedos() {

    return photonenTorpedos;
  }

  /**
   * Setzt einen neuen Wert fuer photonentorpedos.
   *
   * @param photonenTorpedos anzahl der Photonentorpedos
   */
  public void setPhotonenTorpedos(final int photonenTorpedos) {

    this.photonenTorpedos = photonenTorpedos;
  }

  /**
   * Gibt den Broadcastcommunicator des Raumschiffs zurueck.
   *
   * @return {link broadcastCommunicator}
   */
  public static List<String> getBrodcastCommunicator() {

    return BROADCAST_COMMUNICATOR;
  }


  /**
   * Gibt die Anzahl der Reperaturandroiden zurueck.
   *
   * @return reperaturAndroiden
   */
  public int getReperaturAndroiden() {

    return reperaturAndroiden;
  }

  /**
   * Setzt die Anzahl der Reperaturandroiden.
   *
   * @param reperaturAndroiden neuer Wert
   */
  public void setReperaturAndroiden(final int reperaturAndroiden) {

    this.reperaturAndroiden = reperaturAndroiden;
  }

  /**
   * Erstellt eine neue Instanz der Klasse raumschiff und Instanziert alle werte.
   *
   * @param name                der name des Raumschiffs
   * @param huelle              Wert fuer die Huelle
   * @param schilde             Wert fuer die Schilde
   * @param lebenserhaltung     Wert fuer die Lebenserhaltung
   * @param energieversorgung   Wert fuer die Energieversorgung
   * @param ladungsverzeichniss das Ladungsverzeichniss
   * @param photonenTorpedos    Wert fuer geladenen Photonentorpedos
   * @param reperaturAndroiden  Wert fuer die Reperaturandroiden
   */
  public Raumschiff(final String name,
                    final double huelle,
                    final double schilde,
                    final double lebenserhaltung,
                    final double energieversorgung,
                    final Map<String, Ladung> ladungsverzeichniss,
                    final int photonenTorpedos,
                    final int reperaturAndroiden) {

    this();

    this.name = name;
    this.huelle = huelle;
    this.schilde = schilde;
    this.lebenserhaltung = lebenserhaltung;
    this.energieversorgung = energieversorgung;
    this.ladungsverzeichniss = ladungsverzeichniss;
    this.photonenTorpedos = photonenTorpedos;
    this.reperaturAndroiden = reperaturAndroiden;
  }

  /**
   * Instaniziert ein neues Objekt dieser Klasse und fuegt es dem {@link Observer} hinzu.
   */
  public Raumschiff() {
    //empty
  }

  private static void nachrichtSenden(final String nachricht, final String... vals) {

    final StringBuilder res = new StringBuilder();

    final var arr = nachricht.split("%s");

    for (int i = 0; i < vals.length; i++) {
      res.append(arr[i]).append(vals[i]);
    }

    if (!res.toString().contains(arr[arr.length - 1])) {
      res.append(arr[arr.length - 1]);
    }

    nachrichtSenden(res.toString());
  }

  /**
   * Sendet eine Nachricht ueber den Broadcascommunicator an alle Schiffe.
   *
   * @param nachricht die nachricht
   */
  public static void nachrichtSenden(final String nachricht) {

    BROADCAST_COMMUNICATOR.add(nachricht);
    System.out.println(nachricht);
  }

  /**
   * Gibt alle Nachrichten aus dem broadcastCommunicator wieder.
   *
   * @return BROADCAST_COMMUNICATOR
   */
  public static List<String> logbuchEintraegeAusgeben() {

    for (final String s : BROADCAST_COMMUNICATOR) {
      System.out.println(s);
    }

    return BROADCAST_COMMUNICATOR;
  }

  /**
   * Feuert ein Photonentorpedo auf ein Ziel. Wenn keine Photonentorpedos vorhanden sind werden auch
   * keine abgefeuert. Im Ziel Raumschiff wird die Methode {@link Raumschiff#trefferRegistrieren()}
   * aufgerufen.
   *
   * @param ziel Das Ziel Raumschiff
   */
  public void photonenTorpedosAbfeuern(final Raumschiff ziel) {

    nachrichtSenden("[%s] zielt mit Photonentorpedos auf %s", name, ziel.name);
    if (photonenTorpedos > 0) {
      photonenTorpedos--;
      // fire listeners
      nachrichtSenden("[%s] feuert ein Photonentorpedo auf %s", name, ziel.name);
      nachrichtSenden("[%s] Photonentorpedo abgeschossen", name);
      ziel.trefferRegistrieren();
    } else {
      nachrichtSenden("[%s]-=*Click*=-\n", name);
    }
  }

  /**
   * Feuert Phaserkanonen ab sofern genug Energie vorhanden ist. Im Ziel Raumschiff wird die Methode
   * {@link Raumschiff#trefferRegistrieren()} * aufgerufen.
   *
   * @param ziel Das Ziel Raumschiff
   */
  public void phaserAbfeuern(final Raumschiff ziel) {

    // fire listeners
    final double half = .5;
    nachrichtSenden("[%s] zielt mit Phasern auf %s", name, ziel.name);
    if (energieversorgung < half) {
      System.out.printf("[%s] -=*Click*=-\n", name);
    } else {
      energieversorgung -= half;
      System.out.printf("[%s] feuert mit Phasern auf %s\n", name, ziel.name);
      nachrichtSenden("[%s] Phaserkanone abgeschossen", name);
      ziel.trefferRegistrieren();
    }
  }

  /**
   * Kalkuliert den Schaden an Schilden, Huelle und Lebenserhaltung.
   */
  private void trefferRegistrieren() {

    schilde -= .5;
    if (schilde < 0) {
      huelle -= .5;
      energieversorgung -= .5;
    }
    if (huelle <= 0) {
      lebenserhaltung = 0;
      nachrichtSenden("[%s] Lebenserhaltungsystem wurde vollkommen zerstört.", name);
    }

    schilde = schilde < 0 ? 0 : schilde;
    huelle = huelle < 0 ? 0 : huelle;

    nachrichtSenden(ConsoleUtils.ANSI_RED + "[%s] "
                        + ConsoleUtils.ANSI_RESET
                        + "wurde getroffen!\n",
                    name);
  }

  /**
   * Laed eine bestimmte anzahl an Photonentorpedos, sofern es welche im Ladungsverzeichniss des
   * Raumschiffs hat. Werden mehr Photonentorpedos angegben als vorhanden sind werden nur die
   * Vorhandenen Torpedos geladen
   *
   * @param torpedos zu ladenen Photonentorpedos
   */
  public void photonenTorpedoLaden(final int torpedos) {

    final var pt = ladungsverzeichniss.get(RaumschiffUtils.PHOTONEN_TORPEDOS);
    if (ladungsverzeichniss.containsKey(RaumschiffUtils.PHOTONEN_TORPEDOS)) {
      if (pt.getAnzahl() == 0) {
        ladungsverziechnissAufrauemen();
      } else {
        final int anzahl;
        if (torpedos > pt.getAnzahl()) {
          anzahl = pt.getAnzahl();
          setPhotonenTorpedos(getPhotonenTorpedos() - pt.getAnzahl());
          pt.setAnzahl(0);
        } else {
          anzahl = torpedos;
          setPhotonenTorpedos(getPhotonenTorpedos() - torpedos);
          pt.setAnzahl(pt.getAnzahl() - torpedos);
        }
        System.out.printf("[%s] Photonentorpedos eingesetzt", anzahl);
      }

    } else {
      System.out.printf("[%s]keine Photonentorpedos gefunden!\n", name);
      nachrichtSenden("[%s] -=*Click*=-", name);
    }
    photonenTorpedos += torpedos;
  }

  private void ladungsverziechnissAufrauemen() {

    for (final Ladung value : ladungsverzeichniss.values()) {
      if (value.getAnzahl() == 0) {
        ladungsverzeichniss.remove(value.getTyp());
      }
    }

  }

  private int toInt(final boolean val) {

    return val ? 1 : 0;
  }

  /**
   * Repariert die angebenen Systeme. Dabei werden Systeme anhand eines Zufaelligen Wertes zwischen
   * 1 und 100 repariert.
   *
   * @param huelle    boolischer Wert der angibt ob Huelle repariert werden soll.
   * @param schilde   boolischer Wert der angibt ob Schilde repariert werden sollen.
   * @param energie   boolischer Wert der angibt ob Energie repariert werden soll.
   * @param androiden Anzahl der zu verwendeten Androiden
   * @throws InvalidOperationException wenn keine Androiden vorhanden sind oder angegeben wurden,
   *                                   sowie wenn keine Systeme zum reparieren angegeben wurden.
   */
  public void reparieren(final boolean huelle,
                         final boolean schilde,
                         final boolean energie,
                         final int androiden) throws InvalidOperationException {

    final var systems = toInt(huelle) + toInt(schilde) + toInt(energie);

    if (systems < 1) {
      throw new InvalidOperationException("Es wurden keine Systeme ausgewaehlt.");
    }
    if (androiden < 1 || reperaturAndroiden < 1) {
      throw new InvalidOperationException("Es sind nicht genug androiden vorhanden");
    }

    final var rand = new Random();
    final var droids = Math.min(reperaturAndroiden, androiden);
    final double rep = (double) ((rand.nextInt(1, 101) * droids) / systems) / 100;

    this.huelle = huelle ? this.huelle + rep : this.huelle;
    this.schilde = schilde ? this.schilde + rep : this.schilde;
    energieversorgung = energie ? energieversorgung + rep : energieversorgung;

    reperaturAndroiden -= droids;
  }

  /**
   * Gibt den Zustand aller Systeme sowie das Ladungsverzeichnis des Raumschiffs aus.
   */
  public void zustandAusgaben() {

    System.out.println(this);
  }

  private String getColoredString(final int value) {

    return value > .8 ? String.valueOf(value) :
           value > 30
           ? ConsoleUtils.ANSI_YELLOW + value + ConsoleUtils.ANSI_RESET :
           ConsoleUtils.ANSI_RED + value + ConsoleUtils.ANSI_RESET;
  }

  /**
   * Fuegt Ladung dem ladungsverzeichniss hinzu.
   *
   * @param ladung die {@link Ladung}
   */
  public void beladen(final Ladung ladung) {

    ladungsverzeichniss.put(ladung.getTyp(), ladung);
  }

  @Override
  public String toString() {

    final var sb = new StringBuilder(152);
    final var schildZustand = getColoredString((int) (schilde * 100));
    final var huelleZustand = getColoredString((int) (huelle * 100));
    final var energieZustand = getColoredString((int) (energieversorgung * 100));
    final var lebenserhaltungZustand = getColoredString((int) (lebenserhaltung * 100));

    sb.append(ConsoleUtils.ANSI_GREEN).append(name).append(ConsoleUtils.ANSI_RESET)
      .append(":\n").append("--------------------------------").append("\n")
      .append("\tSchilde: ").append(schildZustand).append("%\n")
      .append("\tHuelle: ").append(huelleZustand).append("%\n")
      .append("\tLebenserhaltung: ").append(lebenserhaltungZustand).append(
          "%\n")
      .append("\tEnergieversorgung: ").append(energieZustand).append(
          "%\n")
      .append("\tPhotonentorpedos: ").append(photonenTorpedos).append("\n")
      .append("\tLadung: \n");

    for (final Ladung ladung : ladungsverzeichniss.values()) {

      sb.append("\t\t").append(ladung.getTyp()).append("(").append(
            ladung.getAnzahl())
        .append(")\n");
    }
    return sb.toString();
  }

}
