package Raumschiff.models;

import Raumschiff.utils.RaumschiffUtils;

/**
 * Ladungsklasse eines Photonentorpedos.
 */
public class PhotonenTorpedo extends Ladung {

  /**
   * Erstellt eine neue Ladung des Typs Photonentorpedo.
   *
   * @param anzahl anzahl der Photonentorpedos.
   */
  public PhotonenTorpedo(final int anzahl) {

    super(anzahl, RaumschiffUtils.PHOTONEN_TORPEDOS);
  }

}
