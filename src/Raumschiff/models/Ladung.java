package Raumschiff.models;

/**
 * Klasse Fuer die Ladung.
 */
public class Ladung {

  /**
   * Die Menge.
   */
  private int anzahl;
  /**
   * Der Name der ladung.
   */
  private String typ;

  @Override
  public String toString() {

    final StringBuilder sb = new StringBuilder("models.Ladung{");
    sb.append("anzahl=").append(anzahl);
    sb.append(", typ='").append(typ).append('\'');
    sb.append('}');
    return sb.toString();
  }


  /**
   * Gibt die Anzahl wieder.
   *
   * @return anzahl
   */
  public int getAnzahl() {

    return anzahl;
  }

  /**
   * Setzt die Anzahl.
   *
   * @param anzahl neuer Wert
   */
  public void setAnzahl(final int anzahl) {

    this.anzahl = anzahl;
  }

  /**
   * Gibt den Namen wieder.
   *
   * @return typ
   */
  public String getTyp() {

    return typ;
  }

  /**
   * Setzt den namen der Ladung.
   *
   * @param typ neuer namen
   */
  public void setTyp(final String typ) {

    this.typ = typ;
  }

  /**
   * Erstellt eine neue Instanz von {@link Ladung}.
   */
  public Ladung() {

  }

  /**
   * Erstellt eine neue Instanz von {@link Ladung}.
   *
   * @param anzahl die Menge
   * @param typ    der Name
   */
  public Ladung(final int anzahl, final String typ) {

    this.anzahl = anzahl;
    this.typ = typ;
  }

}
