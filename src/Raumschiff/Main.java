package Raumschiff;

import Raumschiff.models.Ladung;
import Raumschiff.models.PhotonenTorpedo;
import Raumschiff.models.Raumschiff;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import javax.swing.JOptionPane;

/**
 * Hauptprogramm.
 */
public class Main {

  /**
   * Entrypoint.
   *
   * @param args Argumente, es werden keine erwartet.
   */
  public static void main(final String... args) throws InterruptedException {

    System.out.println(welcome());
    Thread.sleep(3000);

    final var klingonen = new Raumschiff("IKS Hegh'ta", 1, 1, 1, 1, new HashMap<>(), 1, 2);
    final var romulaner = new Raumschiff("IRW Khazara", 1, 1, 1, 1, new HashMap<>(), 2, 2);
    final var vulkanier = new Raumschiff("Ni'Var", .5, .8, 1, .8, new HashMap<>(), 0, 5);
    final List<Raumschiff> schiffe = new ArrayList<>();
    schiffe.add(klingonen);
    schiffe.add(romulaner);
    schiffe.add(vulkanier);

    klingonen.beladen(new Ladung(200, "Ferengi Schneckensaft"));
    klingonen.beladen(new Ladung(200, "Bat'leth Klingonen Schwert"));
    romulaner.beladen(new Ladung(50, "Plasma-Waffen"));
    romulaner.beladen(new Ladung(5, "Borg-Schrott"));
    vulkanier.beladen(new Ladung(35, "Forschungssonde"));
    vulkanier.beladen(new PhotonenTorpedo(3));

    zustaendeAusgeben(schiffe);
    Thread.sleep(1000);
    romulaner.phaserAbfeuern(vulkanier);
    Thread.sleep(1000);
    klingonen.photonenTorpedosAbfeuern(vulkanier);
    Thread.sleep(1000);

    zustaendeAusgeben(schiffe);
    beep();
    JOptionPane.showMessageDialog(null, "kaboom");
  }

  private static void beep() throws InterruptedException {

    java.awt.Toolkit.getDefaultToolkit().beep();
  }

  private static void zustaendeAusgeben(final List<Raumschiff> schiffe) {

    for (final Raumschiff raumschiff : schiffe) {
      raumschiff.zustandAusgaben();
    }
  }

  private static String welcome() {

    return "###################################################################################\n"
        + "                                                                                    \n"
        + "  *****   ********      **        *****    *****    *****     ******     ***  **   **\n"
        + "**    ***    **        ****       **  **   **  **   **  **    **       **     **  ** \n"
        + "**           **       **  **      **   **  **   **  **  **    **      **      ** **  \n"
        + "  ***        **      **    **     **  **   **   **  ****     *****   **       ***     \n"
        + "     **      **     **********    *****    **   **  ** **    **      **       ** **   \n"
        + " **    **    **    **        **   **  **   **  **   **  **   **       **      **  **  \n"
        + "   ****      **   **          **  **   **  *****    **   **  ******     ***   **   ** \n"
        + "                                                                                    \n"
        + "###################################################################################";
  }

}
