package Raumschiff.utils;

/**
 * Utility Klasse fuer Consolen Ausgaben.
 */
public final class ConsoleUtils {

  /**
   * Zuruecksetzten der Farbe.
   */
  public static final String ANSI_RESET = "\u001B[0m";
  /**
   * Schwarz.
   */
  public static final String ANSI_BLACK = "\u001B[30m";
  /**
   * Rot.
   */
  public static final String ANSI_RED = "\u001B[31m";
  /**
   * Gruen.
   */
  public static final String ANSI_GREEN = "\u001B[32m";
  /**
   * Gelb.
   */
  public static final String ANSI_YELLOW = "\u001B[33m";
  /**
   * Blau.
   */
  public static final String ANSI_BLUE = "\u001B[34m";
  /**
   * Lila.
   */
  public static final String ANSI_PURPLE = "\u001B[35m";
  /**
   * Cyan.
   */
  public static final String ANSI_CYAN = "\u001B[36m";
  /**
   * Weiß.
   */
  public static final String ANSI_WHITE = "\u001B[37m";

}
