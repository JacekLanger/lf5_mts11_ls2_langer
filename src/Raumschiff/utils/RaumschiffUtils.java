package Raumschiff.utils;

/**
 * Hilfsklasse fuer Raumschiffe. Hier werden Standardwerte festgehalten.
 */
public class RaumschiffUtils {

  /**
   * Bezeichner fuer Photonentorpedos.
   */
  public static final String PHOTONEN_TORPEDOS = "Photonentorpedo";

}
